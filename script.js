console.log('DOM MANIPULATION')



//Finding HTML Elements
console.log(document.getElementById("demo"))
console.log(document.getElementsByTagName("h1"))
console.log(document.getElementsByClassName("title"))

console.log(document.querySelector("#demo"))
console.log(document.querySelector("h1"))
console.log(document.querySelector(".title"))

//Changing HTML Elements

    
    let myH1 = document.querySelector('h1');
    myH1.innerHTML = `Hello World`;
    console.log(myH1)


    document.getElementById("demo").setAttribute("class", "sample")

    document.getElementById("demo").removeAttribute("class")

    //document.querySelector(".title")




    //Event Listener


    let firstName = document.querySelector("#txt-first-name")
    let lastName = document.querySelector("#txt-last-name")
    let fullName = document.querySelector("#span-full-name")



    const updateName = () => {
        let txtFirst = firstName.value
        let txtLast = lastName.value

        fullName.innerHTML = `${txtFirst} ${txtLast}`
    }

    firstName.addEventListener("keyup", updateName)
    lastName.addEventListener("keyup", updateName)

    


   
